package com.pschni.itmotasksmanager.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GroupDto {
    private String letter;
    private Long number;
}
