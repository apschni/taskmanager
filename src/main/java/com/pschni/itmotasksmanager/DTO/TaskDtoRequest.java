package com.pschni.itmotasksmanager.DTO;

import com.pschni.itmotasksmanager.model.Teacher;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class TaskDtoRequest {
    private Teacher teacher;

    private String text;

    private Instant deadline;

    private Long points;
}
