package com.pschni.itmotasksmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItmotasksmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItmotasksmanagerApplication.class, args);
    }

}
