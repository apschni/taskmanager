package com.pschni.itmotasksmanager.controller;

import com.pschni.itmotasksmanager.DTO.GroupDto;
import com.pschni.itmotasksmanager.model.Group;
import com.pschni.itmotasksmanager.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/group")
public class GroupController {

    private final GroupService groupService;

    @PostMapping
    public ResponseEntity<Long> createGroup(@RequestBody GroupDto groupDtoRequest){
        return new ResponseEntity<>(groupService.createGroup(groupDtoRequest), HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public List<GroupDto> getAllGroups(){
        return groupService.geAllGroups();
    }

    @DeleteMapping("/{id}")
    public GroupDto deleteGroup(@PathVariable("id") Long id){
        return groupService.deleteGroupById(id);
    }

    @GetMapping("/{id}")
    public GroupDto getGroupById(@PathVariable("id") Long id){
        return groupService.getGroupById(id);
    }

    @PatchMapping("/{id}")
    public HttpStatus updateGroupInfo(@PathVariable("id") Long id, @RequestBody Group group){
        return groupService.changeGroupInfo(id, group);
    }
}
