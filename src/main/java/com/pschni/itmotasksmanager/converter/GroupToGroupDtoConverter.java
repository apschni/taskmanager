package com.pschni.itmotasksmanager.converter;

import com.pschni.itmotasksmanager.DTO.GroupDto;
import com.pschni.itmotasksmanager.model.Group;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
public class GroupToGroupDtoConverter implements Converter<Group, GroupDto> {

    @Override
    public GroupDto convert(Group group) {
        return GroupDto.builder()
                .number(group.getNumber())
                .letter(group.getLetter())
                .build();
    }
}
