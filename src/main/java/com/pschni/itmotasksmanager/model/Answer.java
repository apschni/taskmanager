package com.pschni.itmotasksmanager.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.File;
import java.time.Instant;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="answer")
public class Answer {
    @Id
    @GeneratedValue
    private Long id;

    private String message;

    @Transient
    private File file;

    private Long points;

    private Instant answeredAt;

    private Boolean isApproved;
}
