package com.pschni.itmotasksmanager.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.File;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="task")
public class Task {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Teacher teacher;

    private String text;

    private Instant deadline;

    private Long points;

    private Boolean isClosed;

    @Transient
    private File file;

    @JoinColumn
    @OneToMany
    private List<Answer> answer;

    public static Boolean isDeadlineEnded(Task task){
        return task.getDeadline().isBefore(Instant.now());
    }
}
