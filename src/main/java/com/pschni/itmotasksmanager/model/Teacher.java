package com.pschni.itmotasksmanager.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="teacher")
public class Teacher {
    @Id
    @GeneratedValue
    private Long id;

    private String fullName;

    private String subject;

    @JoinColumn
    @OneToMany
    private List<Group> groupList;
}
