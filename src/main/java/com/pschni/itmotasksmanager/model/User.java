package com.pschni.itmotasksmanager.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name="usr")
public class User {
    @Id
    private Long id;

    private String fullName;

    @JoinColumn
    @OneToMany
    private List<Task> tasks;
}
