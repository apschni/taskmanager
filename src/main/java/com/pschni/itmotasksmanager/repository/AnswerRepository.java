package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
