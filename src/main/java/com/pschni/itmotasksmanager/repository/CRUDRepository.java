package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.Answer;
import com.pschni.itmotasksmanager.model.Group;
import com.pschni.itmotasksmanager.model.Task;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class CRUDRepository implements ISimpleCRUDRepository{
    private final AnswerRepository answerRepository;
    private final GroupRepository groupRepository;
    private final TaskRepository taskRepository;
    private final TeacherRepository teacherRepository;
    private final UserRepository userRepository;

    @Override
    public Long saveGroup(Group group) {
        return groupRepository.save(group).getId();
    }

    @Override
    public Group findGroupById(Long id) {
        Optional<Group> groupOptional = groupRepository.findById(id);
        return groupOptional.orElse(null);
    }

    @Override
    public void deleteGroupById(Long id) {
        groupRepository.deleteById(id);
    }

    @Override
    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    @Override
    public Task findTaskById(Long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        return taskOptional.orElse(null);
    }

    @Override
    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    @Override
    public Task findTaskByAnswer(Answer answer) {
        return taskRepository.findTaskByAnswerEquals(answer);
    }

    @Override
    public Long saveTask(Task task) {
        return taskRepository.save(task).getId();
    }

    @Override
    public Answer findAnswerById(Long id) {
        Optional<Answer> answerOptional = answerRepository.findById(id);
        return answerOptional.orElse(null);
    }

    @Override
    public Long saveAnswer(Answer answer) {
        return answerRepository.save(answer).getId();
    }
}
