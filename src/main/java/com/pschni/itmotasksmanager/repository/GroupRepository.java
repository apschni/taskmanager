package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
