package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.DTO.GroupDto;
import com.pschni.itmotasksmanager.model.Answer;
import com.pschni.itmotasksmanager.model.Group;
import com.pschni.itmotasksmanager.model.Task;

import java.util.List;

public interface ISimpleCRUDRepository {
    public Long saveGroup(Group group);

    public Group findGroupById(Long id);

    public void deleteGroupById(Long id);

    public List<Group> getAllGroups();

    public Long saveTask(Task task);

    public Task findTaskById(Long id);

    public List<Task> findAllTasks();

    public Task findTaskByAnswer(Answer answer);

    public Answer findAnswerById(Long id);

    public Long saveAnswer(Answer answer);

}
