package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.Answer;
import com.pschni.itmotasksmanager.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
    public Task findTaskByAnswerEquals(Answer answer);
}
