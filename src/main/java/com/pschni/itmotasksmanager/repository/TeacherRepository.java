package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
