package com.pschni.itmotasksmanager.repository;

import com.pschni.itmotasksmanager.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
