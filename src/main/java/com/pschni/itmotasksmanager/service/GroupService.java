package com.pschni.itmotasksmanager.service;

import com.pschni.itmotasksmanager.DTO.GroupDto;
import com.pschni.itmotasksmanager.model.Group;
import com.pschni.itmotasksmanager.repository.CRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GroupService {

    private final ConversionService mvcConversionService;

    private final CRUDRepository crudRepository;


    public Long createGroup(GroupDto groupDtoRequest) {
        return crudRepository.saveGroup(Group.builder()
                .letter(groupDtoRequest.getLetter())
                .number(groupDtoRequest.getNumber())
                .build());
    }

    public GroupDto getGroupById(Long id) {
        Group groupById = crudRepository.findGroupById(id);
        if (groupById == null){
            return null;
        }
        return mvcConversionService.convert(groupById, GroupDto.class);
    }

    public GroupDto deleteGroupById(Long id) {
        Group groupById = crudRepository.findGroupById(id);
        if (groupById == null){
            return null;
        }

        crudRepository.deleteGroupById(id);
        return mvcConversionService.convert(groupById, GroupDto.class);
    }

    public HttpStatus changeGroupInfo(Long id, Group group) {
        Group groupById = crudRepository.findGroupById(id);
        if (groupById == null){
            return HttpStatus.NOT_FOUND;
        }
        crudRepository.saveGroup(Group.builder()
                .id(groupById.getId())
                .number(group.getNumber())
                .letter(group.getLetter())
                .build());
        return HttpStatus.OK;
    }

    public List<GroupDto> geAllGroups() {
        return crudRepository.getAllGroups().stream()
                .map(group -> mvcConversionService.convert(group, GroupDto.class))
                .collect(Collectors.toList());
    }
}
