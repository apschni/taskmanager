package com.pschni.itmotasksmanager.service;

import com.pschni.itmotasksmanager.DTO.TaskDtoRequest;
import com.pschni.itmotasksmanager.model.Answer;
import com.pschni.itmotasksmanager.model.Task;
import com.pschni.itmotasksmanager.repository.CRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TaskService {
    private final CRUDRepository crudRepository;

    public Long createTask(TaskDtoRequest taskDtoRequest) {
        Task task = Task.builder()
                .teacher(taskDtoRequest.getTeacher())
                .text(taskDtoRequest.getText())
                .deadline(taskDtoRequest.getDeadline())
                .points(taskDtoRequest.getPoints())
                .isClosed(Boolean.FALSE)
                .file(null)
                .answer(null)
                .build();

        return crudRepository.saveTask(task);
    }

    public List<Task> getAllTasksOrderedByDeadline(){
        return crudRepository.findAllTasks().stream()
                .sorted(Comparator.comparing(Task::getDeadline))
                .collect(Collectors.toList());
    }

    public Task updateTaskWithFile(Long id, File file){
        Task task = crudRepository.findTaskById(id);
        if (task == null){
            return null;
        }
        task.setFile(file);
        crudRepository.saveTask(task);
        return task;
    }

    public Task openTask(Long id){
        Task task = crudRepository.findTaskById(id);
        if (task == null){
            return null;
        }
        task.setIsClosed(Boolean.FALSE);
        crudRepository.saveTask(task);
        return task;
    }

    public Task closeTask(Long id){
        Task task = crudRepository.findTaskById(id);
        if (task == null){
            return null;
        }
        task.setIsClosed(Boolean.TRUE);
        crudRepository.saveTask(task);
        return task;
    }

    public List<Answer> getAllAnswers(Long id){
        Task task = crudRepository.findTaskById(id);
        if (task == null){
            return null;
        }
        return task.getAnswer();
    }

    public Task approveTask(Long answerId){
        Answer answer = crudRepository.findAnswerById(answerId);
        if (answer == null){
            return null;
        }
        Task task = crudRepository.findTaskByAnswer(answer);
        answer.setIsApproved(Boolean.TRUE);
        task.setIsClosed(Boolean.TRUE);
        crudRepository.saveTask(task);
        crudRepository.saveAnswer(answer);

        return task;
    }

    public Answer rateAnswer(Long answerId, Long points){
        Answer answer = crudRepository.findAnswerById(answerId);
        if (answer == null){
            return null;
        }
        answer.setPoints(points);
        crudRepository.saveAnswer(answer);
        return answer;
    }

    public Task answerTask(Long id, Answer answer){
        Task task = crudRepository.findTaskById(id);
        if (task == null || task.getIsClosed()){
            return task;
        }

        for (Answer answerFromTask : task.getAnswer()) {
            if (answerFromTask.getIsApproved()){
                task.setIsClosed(Boolean.TRUE);
                crudRepository.saveTask(task);
                return task;
            }
        }
        answer.setAnsweredAt(Instant.now());
        task.getAnswer().add(answer);
        crudRepository.saveTask(task);
        return task;
    }
}
