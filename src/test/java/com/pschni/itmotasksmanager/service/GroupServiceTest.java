package com.pschni.itmotasksmanager.service;


import com.pschni.itmotasksmanager.DTO.GroupDto;
import com.pschni.itmotasksmanager.model.Group;
import com.pschni.itmotasksmanager.repository.CRUDRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GroupServiceTest {
    @Mock
    private ConversionService mvcConversionService;

    @Mock
    private CRUDRepository crudRepository;

    @InjectMocks
    private GroupService groupService;


    Group testGroup = Group.builder()
            .id(1L)
            .letter("M")
            .number(3105L)
            .build();
    GroupDto testGroupDto = GroupDto.builder()
            .letter("M")
            .number(3105L)
            .build();

    @Test
    void createGroup() {
        when(crudRepository.saveGroup(any())).thenReturn(1L);

        Long result = groupService.createGroup(testGroupDto);

        verify(crudRepository, times(1)).saveGroup(any());
        Assertions.assertEquals(1L, result);
    }

    @Test
    void getGroupById_notExists() {
        when(crudRepository.findGroupById(1L)).thenReturn(null);

        GroupDto groupById = groupService.getGroupById(1L);

        Assertions.assertNull(groupById);
        verify(mvcConversionService, never()).convert(any(), eq(GroupDto.class));
    }

    @Test
    void getGroupById_exists() {
        when(crudRepository.findGroupById(1L)).thenReturn(testGroup);
        when(mvcConversionService.convert(testGroup, GroupDto.class)).thenReturn(testGroupDto);

        GroupDto groupById = groupService.getGroupById(1L);
        Assertions.assertEquals(groupById, testGroupDto);
    }

    @Test
    void deleteGroupById_exists() {
        when(crudRepository.findGroupById(any())).thenReturn(testGroup);
        when(mvcConversionService.convert(testGroup, GroupDto.class)).thenReturn(testGroupDto);

        GroupDto groupDto = groupService.deleteGroupById(1L);

        verify(crudRepository, times(1)).deleteGroupById(1L);
        Assertions.assertEquals(groupDto, testGroupDto);

    }

    @Test
    void deleteGroupById_notExists() {
        when(crudRepository.findGroupById(1L)).thenReturn(null);

        GroupDto groupDto = groupService.deleteGroupById(1L);

        Assertions.assertNull(groupDto);
        verify(crudRepository, never()).deleteGroupById(any());
    }

    @Test
    void changeGroupInfo_notExists() {
        when(crudRepository.findGroupById(any())).thenReturn(null);

        HttpStatus httpStatus = groupService.changeGroupInfo(1L, testGroup);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, httpStatus);
        verify(crudRepository, never()).saveGroup(any());
    }

    @Test
    void changeGroupInfo_exists() {
        when(crudRepository.findGroupById(any())).thenReturn(testGroup);

        HttpStatus httpStatus = groupService.changeGroupInfo(1L, testGroup);

        Assertions.assertEquals(HttpStatus.OK, httpStatus);
        verify(crudRepository, times(1)).saveGroup(any());
    }

    @Test
    void geAllGroups() {
        Group testGroup2 = Group.builder()
                .id(2L)
                .letter("Z")
                .number(3301L)
                .build();

        GroupDto testGroupDto2 = GroupDto.builder()
                .letter("Z")
                .number(3301L)
                .build();

        when(crudRepository.getAllGroups()).thenReturn(List.of(
                testGroup,
                testGroup2
        ));

        when(mvcConversionService.convert(testGroup, GroupDto.class)).thenReturn(testGroupDto);
        when(mvcConversionService.convert(testGroup2, GroupDto.class)).thenReturn(testGroupDto2);

        List<GroupDto> groupDtos = groupService.geAllGroups();

        Assertions.assertEquals(List.of(
                testGroupDto,
                testGroupDto2
        ), groupDtos);
    }
}
